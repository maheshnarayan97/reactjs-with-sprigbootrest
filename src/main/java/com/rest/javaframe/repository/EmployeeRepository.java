package com.rest.javaframe.repository;
import com.rest.javaframe.model.Employee;
import org.springframework.data.repository.CrudRepository;
public interface EmployeeRepository extends CrudRepository<Employee, Long> {
}
