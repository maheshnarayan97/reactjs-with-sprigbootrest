package com.rest.javaframe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaframeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaframeApplication.class, args);
	}

}
