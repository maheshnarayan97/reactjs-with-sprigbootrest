import React from 'react';
import {Card, Form, Button, Col} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSave, faPlusSquare, faUndo, faList, faEdit} from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import MyToast from './MyToast';

class Book extends React.Component{
    constructor(props){
        super(props);
        this.state = this.initialState;
        this.state.show = false;
        this.bookChange = this.bookChange.bind(this);
        this.submitBook =this.submitBook.bind(this);
    }

    initialState = {
      id:'',first_name:'',last_name:'',address:'',language:''
    }

    componentDidMount() {
      const bookId = +this.props.match.params.id;
      if(bookId){
       this.findBookById(bookId);
      }
    }


  findBookById = (bookId) => {
    axios.get("http://localhost:8080/api/list/"  +bookId)
    .then(response  => {
       console.log(response);
      if(response.data != null){
        
        this.setState({
         
          id: response.data.id, 
          first_name: response.data.first_name,
          last_name: response.data.last_name,
          address: response.data.address,
          language:response.data.language
        });
      }
  
    }).catch((error) =>{
      console.error("error - " +error);
    });  

  }
    submitBook = event => {
      event.preventDefault();

      const book = {

        first_name:this.state.first_name,
        last_name:this.state.last_name,
        address:this.state.address,
        language:this.state.language
           
      };

      axios.post("http://localhost:8080/api/save", book)
      .then(response => {
        if(response.data != null){
          this.setState({"show":true,"method":"post"});
          setTimeout(() => this.setState({"show":false}),3000);
          }else{
            this.setState({"show":false});
          }
      });
      this.setState(this.initialState);
    }
    updateBook = event => {
      event.preventDefault();

      const book = {
        id: this.state.id,
        first_name:this.state.first_name,
        last_name:this.state.last_name,
        address:this.state.address,
        language:this.state.language
           
      };

      axios.put("http://localhost:8080/api/save", book)
      .then(response => {
        console.log(response);
        if(response.data != null){
          this.setState({"show":true,"method":"put"});
          setTimeout(() => this.setState({"show":false}),3000);
          setTimeout(() => this.booklist(),3000);
          }else{
            this.setState({"show":false});
          }
      });
      this.setState(this.initialState);
    }

    reset = ()=>{
      this.setState(() => this.initialState);
    }
    bookChange = event => {
      this.setState({
        [event.target.name]:event.target.value
      });
    }

    booklist = () =>{
      return this.props.history.push("/list");
    };
    
   render(){
     const {first_name,last_name,address,language} = this.state;
    

 return(
     <div>
       <div style={{"display":this.state.show ? "block": "none"}}>
         <MyToast show = {this.state.show} message = {this.state.method === "put" ? "Employee Updated successfully" : "Employee Registered successfully"}  type = {"success"}/>
       </div>
       <Card className={"border border-dark bg-dark text-white"}>
                    <Card.Header>
                        <FontAwesomeIcon icon={this.state.id ? faEdit : faPlusSquare} /> {this.state.id ? "Update Employee" : "Add New Employee"}
                    </Card.Header>
                    <Form onReset={this.resetBook} onSubmit={this.state.id ? this.updateBook : this.submitBook} id="bookFormId">
                        <Card.Body>
                            <Form.Row>
                                <Form.Group as={Col} controlId="firs_tname">
                                    <Form.Label>First Name</Form.Label>
                                    <Form.Control required autoComplete="off"
                                        type="test" name="first_name"
                                        value={first_name} onChange={this.bookChange}
                                        className={"bg-dark text-white"}
                                        placeholder="Enter The First Name" />
                                </Form.Group>
                                <Form.Group as={Col} controlId="last_name">
                                    <Form.Label>Last Name</Form.Label>
                                    <Form.Control required autoComplete="off"
                                        type="test" name="last_name"
                                        value={last_name} onChange={this.bookChange}
                                        className={"bg-dark text-white"}
                                        placeholder="Enter The Last Name" />
                                </Form.Group>
                            </Form.Row>
                            <Form.Row>
                                <Form.Group as={Col} controlId="address">
                                    <Form.Label>Address</Form.Label>
                                   
                                        <Form.Control required autoComplete="off"
                                            type="test" name="address"
                                            value={address} onChange={this.bookChange}
                                            className={"bg-dark text-white"}
                                            placeholder="Enter The Address" />
                                  </Form.Group>
                            
                              <Form.Group as={Col} controlId="language">
                                  <Form.Label>Language</Form.Label>
                                   <Form.Control as="select" name="language" 
                                   value={language} onChange={this.bookChange} className={"bg-dark text-white"} custom>
                                   <option>English</option>
                                   <option>Malayalam</option>
                                    <option>Hindi</option>
                                   <option>French</option>
                                  <option>Tamil</option>
                                   
      
                            </Form.Control>
                          </Form.Group>
                          </Form.Row>
       
   
                        
                           
                        </Card.Body>
                        <Card.Footer style={{"textAlign":"right"}}>
                            <Button size="sm" variant="success" type="submit">
                                <FontAwesomeIcon icon={faSave} /> {this.state.id ? "Update" : "Save"}
                            </Button>{' '}
                            <Button size="sm" variant="info" type="reset">
                                <FontAwesomeIcon icon={faUndo} /> Reset
                            </Button>{' '}
                            <Button size="sm" variant="info" type="button" onClick={this.booklist.bind()}>
                                <FontAwesomeIcon icon={faList} /> Book List
                            </Button>
                        </Card.Footer>
                    </Form>
                </Card></div>
 );
 }
}

export default Book;