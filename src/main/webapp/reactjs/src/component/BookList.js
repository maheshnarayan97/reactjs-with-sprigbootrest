import React from 'react';
import {Card, Table, ButtonGroup, Button} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faList, faTrash, faEdit} from '@fortawesome/free-solid-svg-icons';
import axios from 'axios';
import MyToast from './MyToast';
import {Link} from 'react-router-dom';

class BookList extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      books :[]
    };
  }

  componentDidMount(){
       axios.get("http://localhost:8080/api/list")
       .then(Response => Response.data)
       .then((data) => {
         this.setState({books: data});
       });
  }

 deleteBook = (bookId) => {
     axios.delete("http://localhost:8080/api/delete/"+bookId)
     .then(response => {
       if(response.data != null){
        this.setState({"show":true});
        setTimeout(() => this.setState({"show":false}),3000);
      
         this.setState({
           books: this.state.books.filter(book => book.id !== bookId)
         });
       }else{
        this.setState({"show":false});
      }
     })
 };


 render(){

 return(
   <div>
     <div style={{"display":this.state.show ? "block": "none"}}>
         <MyToast show = {this.state.show} message = {"Employee Deleted successfully"} type = {"danger"}/>
       </div>
   
   <Card className={"border border-dark bg-dark text-white"}>
     <Card.Header><FontAwesomeIcon icon={faList} />Employee List</Card.Header>
     <Card.Body>
       <Table bordered hover striped variant="dark">
       <thead>
    <tr>
    
      <th>FIRST NAME</th>
      <th>LAST NAME</th>
      <th>ADDRESS</th>
      <th>LANGUAGE</th>

      <th>ACTIONS</th>
    </tr>
    </thead>
<tbody>
  {
    this.state.books.length === 0 ?
    <tr align="center">
      <td colSpan="6">Employees  are Not available</td>
    </tr>:
    this.state.books.map((book) => (
      <tr key={book.id}>
        
        <td>{book.first_name}</td>
        <td>{book.last_name}</td>
        <td>{book.address}</td>
        <td>{book.language}</td>
        <td>
          <ButtonGroup>
          <Link to={"edit/"+book.id} className="btn btn-sm outline-primary"><FontAwesomeIcon icon={faEdit} /></Link>
      
            <Button size="sm" variant="outline-danger" onClick={this.deleteBook.bind(this, book.id)} ><FontAwesomeIcon icon={faTrash} /></Button>
          </ButtonGroup>
        </td>
      </tr>
    ))
  }
    

  </tbody>
       </Table>
     </Card.Body>

   </Card></div>
 );
 }
}

export default BookList;